import { html } from 'lit-html'

const Keyboard = store => html`
  <div class="keyboard">
    <div class="key" on-click="${_ => store.moveLeft()}">⬅</div>
    <div class="key" on-click="${_ => store.moveRight()}">➡</div>
    <div class="key" on-click="${_ => store.moveUp()}">⬆</div>
    <div class="key" on-click="${_ => store.moveDown()}">⬇</div>
  </div>
`

export { Keyboard }
