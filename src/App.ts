import { html } from 'lit-html'
import { Field } from './Field'
import { Keyboard } from './Keyboard'

const App = store => html`
  <h1>${store.titel}</h1>
  <div class="board">${store.items.map(Field)}</div>
  ${Keyboard(store)}
`
export { App }
