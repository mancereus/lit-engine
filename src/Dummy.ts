import { html } from 'lit-html'

const Dummy = (store) => html`
<p><input value="${store.name}" on-input="${(event) => store.setName(event.target.value)}" /></p>
<p><input value="${store.name}" on-input="${(event) => store.setName(event.target.value)}" /></p>
<h1>Hello, ${store.name}!</h1>
<h1>Hello, ${store.name}!</h1>
`
export { Dummy }
