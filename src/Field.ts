import { html } from 'lit-html'
import { store } from './model/Store'

const Field = (item, idx) =>
  html`
    <div class="field sink-${item.value}" on-click="${_ => item.sink()}">
      ${store.pos === idx ? 'X' : ''} ${store.target >= 0 && store.target !== store.pos && store.target === idx ? 'Y' : ''}
    </div>
  `

export { Field }
