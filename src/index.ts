import { render } from 'lit-html'
import { autorun } from 'mobx'
import './style.css'
import { App } from './App'
import { store } from './model/Store'

// const query = `{
//     Movie(title: "Inception") {
//       releaseDate
//       actors {
//         name
//       }
//     }
//   }`

// request('https://api.graph.cool/simple/v1/movies', query).then((data) => console.log(data))

autorun(() => {
  render(App(store), document.getElementById('root'))
})
