import { types } from 'mobx-state-tree'

const FieldModel = types
  .model('Field', {
    value: types.number,
  })
  .actions((selfField) => ({
    sink() {
      selfField.value = Math.min(selfField.value + 1, 4)
    },
  }))

export { FieldModel }
