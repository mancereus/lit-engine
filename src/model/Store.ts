import { types } from 'mobx-state-tree'
import { FieldModel } from './FieldModel'
import { start } from './Start'
import hotkeys from 'hotkeys-js'

const Store = types
  .model('Store', {
    name: types.string,
    items: types.array(FieldModel),
    pos: 0,
    target: -1,
    counter: 0,
  })
  .actions(self => ({
    setName(name) {
      self.name = name
    },
    dead() {
      self.setName('ICE - Dead')
    },
    setNewTarget() {
      if (self.target === -1) {
        let height = -1
        while (height === -1 || height === 4) {
          self.target = Math.floor(Math.random() * 100)
          height = self.items[self.target].value
          console.log('height: ', height)
        }
      }
    },
    sinkRandom() {
      let idx = 0
      while ([0, 9, 99, 90].includes(idx)) {
        idx = Math.floor(Math.random() * 100)
      }
      self.items[idx] && self.items[idx].sink()
      if (idx === self.pos && self.items[idx].value === 4) {
        self.dead()
      }
    },
    moveable(newPos): boolean {
      console.log('pos value', self.items[newPos] && self.items[newPos].value)
      return self.items[newPos] && self.items[newPos].value > -1 && self.items[newPos].value < 4
    },
    checkTarget() {
      if (self.pos === self.target) {
        self.target = -1
        self.counter++
        self.setNewTarget()
      }
    },
    moveDown() {
      const newPos = self.pos + 10
      if (newPos < 100 && self.moveable(newPos)) {
        self.pos = newPos
      }
      self.checkTarget()
    },
    moveUp() {
      const newPos = self.pos - 10
      if (newPos > -1 && self.moveable(newPos)) {
        self.pos = newPos
      }
      self.checkTarget()
    },
    moveRight() {
      const newPos = self.pos + 1
      if (newPos < 100 && Math.floor(newPos / 10) === Math.floor(self.pos / 10) && self.moveable(newPos)) {
        self.pos = newPos
      }
      self.checkTarget()
    },
    moveLeft() {
      const newPos = self.pos - 1
      if (newPos > -1 && Math.floor(newPos / 10) === Math.floor(self.pos / 10) && self.moveable(newPos)) {
        self.pos = newPos
      }
      self.checkTarget()
    },
  }))
  .views(self => ({
    get titel() {
      return self.name + ' - gerettet: ' + self.counter
    },
  }))

const store = Store.create(start)

store.setNewTarget()
setInterval(() => store.sinkRandom(), 200)

hotkeys('down', e => store.moveDown())
hotkeys('up', e => store.moveUp())
hotkeys('left', e => store.moveLeft())
hotkeys('right', e => store.moveRight())

export { store }
